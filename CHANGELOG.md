# Change Log

Toutes les modifications apportées au projet seront documentées dans ce fichier.

Le format est basé sur le modèle [Keep a Changelog](http://keepachangelog.com/) et adhère aux principes du [Semantic Versioning](http://semver.org/).

## [4.0.0] - 2024-01-05

### Evolutions
- Changement de la license open source vers MIT
- Prise en charge de la diffusion avec packagist

## [3.0.0] - 09/01/2023

### Ajouts
- Changement de la license opensource de la CeCILL v2 vers GNU Affero v3
- Migration php 7.4

## [2.0.3] - 22/01/2021

### Ajouts
- Changement de nom du projet phpgedooo_client

## [2.0.1] - 12/06/2017

### Correction
- Correction de la fonction de convertion xml RPC2 (_xmlRpc2Convert) : Utility/Converter/FusionConvConverterCloudooo.php

## [2.0.0] - 08/03/2017

### Ajouts
- Utilisation des namespaces pour les dépendances
- Mise en place du fork phpgedooo_client
- Compatibilité PHP 7

### Suppression
- Suppression des rétrocompatibilités pour des versions précédentes de la librairie php de gedooo


## [1.0.0] - 15/01/2016

### Ajouts
- Mise en production

## [0.9.0]

### Ajouts
- Création du plugin CakePHP FusionConv
