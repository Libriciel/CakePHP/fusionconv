<?php

/**
 * La classe FusionConvAbstractConverter ...
 *
 * @package FusionConv
 * @subpackage Utility.Converter
 */
abstract class Converter implements ConverterInterface
{
    /**
     * Retourne la valeur lue dans le path de self::$configure sous la clé
     * $configureKey ou la valeur par défaut, tant que la valeur est null.
     *
     * @todo Se servir du plugin Configured, qui reste à écrire
     *
     * @param array $configured
     * @param string $configureKey
     * @param mixed $currentValue
     * @return mixed
     */
    protected static function _configured($configured, $configureKey, $currentValue = null)
    {
        if (is_null($currentValue)) {
            $configuredPath = Hash::get($configured, "{$configureKey}.path");
            $currentValue = Configure::read($configuredPath);

            if (is_null($currentValue)) {
                $currentValue = Hash::get($configured, "{$configureKey}.default");
            }
        }

        return $currentValue;
    }
}
